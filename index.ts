// Parte 1
// Cree una versión REST de la tarea anterior utilizando express.js y TS. FALTA ESTO
// La API debería poder manejar:

// - Creación de una publicación de blog. -----OK
// - Actualización de una publicación de blog.  ----OK
// - Eliminar una publicación de blog. ------OK
// - Recuperar una sola publicación de blog basada en un identificador. -----OK
// - Recupere una lista de publicaciones de blog ordenadas de la más reciente a la más antigua. -----OK
// - Se debe utilizar PostgreSQL como solución de almacenamiento. ------OK
// - Todas las publicaciones deben almacenarse en la base de datos. -------OK
// - No se necesitan autenticaciones ni restricciones de seguridad. -------OK
// - Creación de comentarios para publicaciones. ------OK
// - Creación de la capacidad de agregar etiquetas a las publicaciones. OK
// - Aplicar al menos 2 middleware. -----OK
// - Validar los datos entrantes. -----OK
// - Manejar errores. 50/50 

// Parte 2
// - Proporcione un diagrama de relación de entidades de base de datos de la base de datos que está utilizando.
// - El ERD puede ser un enlace, una imagen, un pdf o cualquier cosa que podamos ver a nivel detallado. OK

// Crédito adicional: Su programa implementa migraciones de código de modo que 
// tener una base de datos vacía debería ser el único requisito. Se proporcionan scripts para sembrar y 
//crear todas las tablas necesarias. Algunas referencias:

// https://typeorm.io/#/migrations
// https://db-migrate.readthedocs.io/en/latest/Getting%20Started/usage/

const express = require('express'); //importamos módulo express
const app = express();
const port = 3000; // definimos el puerto
const routes = require('./src/routes/index'); // importamos el archivo index que contiene las rutas
const bodyParser = require('body-parser'); // importamos middleware bodyParse
const morgan = require('morgan'); // importamos middleware morgan

//Middleware Recibir información por application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// Middleware para converitr informacion del body a json
app.use(bodyParser.json())

app.use(morgan('dev')); // MIDDLEWARE PARA MOSTRAR INFORMACIÓN DE LA PETICIÓN


app.use('/api', routes) // Definir las rutas de la api

app.listen(port,()=>{
    console.log(`Conexión establecida ${port}`)
})