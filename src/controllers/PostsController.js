"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var db = require('../database/db');
module.exports = {
    createPost: function (req, res) {
        // Condicional para validar que se ingresen todos los datos requeridos
        if (req.body.title !== '' && req.body.author !== '' && req.body.content !== '' && req.body.tags !== '') {
            // Consulta para insertar datos a la base de datos
            db.query("INSERT INTO POSTS (TITLE, AUTHOR, CONTENT, TAGS) VALUES ('" + req.body.title + "', '" + req.body.author + "', '" + req.body.content + "', '" + req.body.tags + "');", function (err, result) {
                if (err) { // Condicional para obtención de error
                    res.status(500).send({
                        message: 'ERROR DEL SERVIDOR'
                    });
                }
                else { //Indicar resultados de consulta
                    res.status(200).json(result.rows);
                }
            });
        }
        else { // Mnesaje de error al no llenar todos los campos
            res.status(400).send({
                message: 'ERROR: DEBE LLENAR TODOS LOS CAMPOS'
            });
        }
    },
    updatePost: function (req, res) {
        // Consulta que se enviará a la base de datos
        db.query("SELECT * from POSTS WHERE ID_POST='" + req.params.id + "'", function (err, result) {
            if (err) { // Condicional para obtención de error
                res.status(500).send({
                    message: 'SE PRESENTÓ UN ERROR AL HACER UNA CONSULTA A LA BASE DE DATOS'
                });
            }
            else { // Indicaciones si no encuentra error
                var object = result.rows[0]; // Definimos un objeto con los resultados de la consulta
                if (req.body.title !== '') { // Condicional para verifiacr que el título no esté vacío
                    object.title = req.body.title;
                }
                if (req.body.author !== '') { // Condicional para verifiacr que el autor no esté vacío
                    object.author = req.body.author;
                }
                if (req.body.content !== '') { // Condicional para verifiacr que el contenido no esté vacío
                    object.content = req.body.content;
                }
                if (req.body.tags !== '') { // Condicional para verifiacr que los tags no estén vacío
                    object.tags = req.body.tags;
                }
                // Consulta para actualizar datos en la base de datos
                db.query("UPDATE POSTS SET TITLE='" + object.title + "', AUTHOR='" + object.author + "', CONTENT='" + object.content + "', TAGS='" + object.tags + "' WHERE ID_POST='" + req.params.id + "'", function (err, result) {
                    if (err) { // Condicional para mostrar un error
                        res.status(400).send({
                            message: 'SE PRESENTÓ UN ERROR AL ACTUALIZAR DATOS'
                        });
                    }
                    else {
                        res.status(200).json(result.rows);
                    }
                });
            }
        });
    },
    deletePost: function (req, res) {
        // Consulta para eliminar un post de la base de datos
        db.query("DELETE FROM POSTS WHERE ID_POST='" + req.params.id + "'", function (err, result) {
            if (err) { // Condicional para mostrar un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            }
            else {
                if (result.rowCount === 0) { // Condicional para verificar que se hayan obtenido resultados en la consulta
                    res.status(500).send({
                        message: 'NO SE ENCONTRÓ NINGUN POST CON LA ID INDICADA'
                    });
                }
                else {
                    res.status(200).json(result.rows);
                }
            }
        });
    },
    getPost: function (req, res) {
        // Consulta para seleccionar datos de la base de datos
        db.query("SELECT * FROM POSTS WHERE ID_POST='" + req.params.id + "'", function (err, result) {
            if (err) { // Condicional para mostara un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            }
            else {
                if (result.rowCount === 0) { // Condicional para verificar que se hayan obtenido resultados en la consulta
                    res.status(400).send({
                        message: 'NO SE ENCONTRÓ NINGUN POST CON LA ID INDICADA'
                    });
                }
                else {
                    res.status(200).json(result.rows);
                }
            }
        });
    },
    orderPost: function (req, res) {
        // Consulta para seleccionar datos de la base de datos
        db.query("SELECT * FROM POSTS ORDER BY DATE DESC", function (err, result) {
            if (err) { // Condicional para mostar un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            }
            else {
                res.status(200).json(result.rows);
            }
        });
    }
};
