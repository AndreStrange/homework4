"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var db = require('../database/db');
module.exports = {
    createComment: function (req, res) {
        // Consulta para buscar el ID del post en la  datos
        db.query("SELECT * FROM POSTS WHERE ID_POST='" + req.body.id_post + "'", function (err, result) {
            if (err) { // Condicional para mostrar un error
                res.status(500).send({
                    message: 'ERROR'
                });
            }
            else {
                if (result.rowCount !== 0) { // Condicional para verificar que la consulta no esté vacía
                    if (req.body.conten !== '' && req.body.id_post !== '') { // Condicional para validar que se ingresen todos los datos
                        // Consulta para insertar datos en la base de datos
                        db.query("INSERT INTO COMMENTS (CONTENT, ID_POST) VALUES ('" + req.body.content + "', '" + req.body.id_post + "')", function (err, result) {
                            if (err) { // condicional para mostrar errores
                                res.status(500).send({
                                    message: 'ERROR DEL SERVIDOR'
                                });
                            }
                            else {
                                res.status(200).json(result.rows);
                            }
                        });
                    }
                    else { // Condicional para indicar el error que debe llenar los campos
                        res.status(400).send({
                            message: 'ERROR: DEBE LLENAR TODOS LOS CAMPOS'
                        });
                    }
                }
                else { // Condicional para indicar que no se encontró ningun post con el id
                    res.status(400).send({
                        message: 'NO SE ENCONTRÓ NINGUN POST CON EL ID INDICADO'
                    });
                }
            }
        });
    },
    deleteComment: function (req, res) {
        // Consulta para eliminar un comentario de la base de datos
        db.query("DELETE FROM COMMENTS WHERE ID_COMMENT='" + req.params.id + "'", function (err, result) {
            if (err) { // Condicional para mostrar un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            }
            else {
                if (result.rowCount === 0) { // Condicional para verificar que la consulta no esté vacía
                    res.status(500).send({
                        message: 'NO SE ENCONTRÓ NINGUN COMENTARIO CON LA ID INDICADA'
                    });
                }
                else {
                    res.status(200).json(result.rows);
                }
            }
        });
    },
    getComment: function (req, res) {
        // Consulta para obtener un comentario en la base de datos
        db.query("SELECT * FROM COMMENTS WHERE ID_COMMENT='" + req.params.id + "'", function (err, result) {
            if (err) { // Condicional para mostrar un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            }
            else {
                if (result.rowCount === 0) { // Condicional para verificar que la consulta no esté vacía
                    res.status(500).send({
                        message: 'NO SE ENCONTRÓ NINGUN COMENTARIO CON LA ID INDICADA'
                    });
                }
                else {
                    res.status(200).json(result.rows);
                }
            }
        });
    },
    getAllComments: function (req, res) {
        // Consulta para obtener comentarios
        db.query("SELECT * FROM COMMENTS WHERE ID_POST='" + req.params.id + "'", function (err, result) {
            if (err) { // Condicional para mostrar errores
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            }
            else {
                if (result.rowCount === 0) { // Condicional para verificar que la consulta no esté vacía
                    res.status(500).send({
                        message: 'NO SE ENCONTRÓ NINGUN COMENTARIO EN EL POST'
                    });
                }
                else {
                    res.status(200).json(result.rows);
                }
            }
        });
    }
};
