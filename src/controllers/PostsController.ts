export{}
const db = require('../database/db');

module.exports={

    createPost: (req: any,res: any)=>{ // Método para crear un post
        // Condicional para validar que se ingresen todos los datos requeridos
        if(req.body.title!=='' && req.body.author!=='' && req.body.content!=='' && req.body.tags!==''){ 
            // Consulta para insertar datos a la base de datos
            db.query(`INSERT INTO POSTS (TITLE, AUTHOR, CONTENT, TAGS) VALUES ('${req.body.title}', '${req.body.author}', '${req.body.content}', '${req.body.tags}');`, (err: any,result: any)=>{
                
                if(err){ // Condicional para obtención de error
                    res.status(500).send({
                        message: 'ERROR DEL SERVIDOR'
                    });
                }else{ //Indicar resultados de consulta
                    res.status(200).json(result.rows)
                }
            })
            
        }else{ // Mnesaje de error al no llenar todos los campos
            res.status(400).send({
                message: 'ERROR: DEBE LLENAR TODOS LOS CAMPOS'
            })
        }

    },

    updatePost: (req: any,res: any) =>{ // Método para actualizar un post

        // Consulta que se enviará a la base de datos
        db.query(`SELECT * from POSTS WHERE ID_POST='${req.params.id}'`,(err: any,result: any)=>{ 

            if(err){ // Condicional para obtención de error
                res.status(500).send({
                    message: 'SE PRESENTÓ UN ERROR AL HACER UNA CONSULTA A LA BASE DE DATOS'
                });
            }else{ // Indicaciones si no encuentra error
                let object = result.rows[0] // Definimos un objeto con los resultados de la consulta

                if(req.body.title!==''){ // Condicional para verifiacr que el título no esté vacío
                    object.title=req.body.title
                }
                if(req.body.author!==''){ // Condicional para verifiacr que el autor no esté vacío
                    object.author=req.body.author
                }
                if(req.body.content!==''){ // Condicional para verifiacr que el contenido no esté vacío
                    object.content=req.body.content
                }
                if(req.body.tags!==''){ // Condicional para verifiacr que los tags no estén vacío
                    object.tags=req.body.tags
                }

                // Consulta para actualizar datos en la base de datos
                db.query(`UPDATE POSTS SET TITLE='${object.title}', AUTHOR='${object.author}', CONTENT='${object.content}', TAGS='${object.tags}' WHERE ID_POST='${req.params.id}'`,(err: any,result: any)=>{

                    if(err){ // Condicional para mostrar un error
                        res.status(400).send({
                            message: 'SE PRESENTÓ UN ERROR AL ACTUALIZAR DATOS'
                        });
                    }else{ 
                        res.status(200).json(result.rows);
                    }

                })

            }

        })

    },

    deletePost: (req: any,res: any)=>{ // Método para la eliminación de posts

        // Consulta para eliminar un post de la base de datos
        db.query(`DELETE FROM POSTS WHERE ID_POST='${req.params.id}'`,(err: any,result: any)=>{

            if(err){ // Condicional para mostrar un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            }else{
                if(result.rowCount===0){ // Condicional para verificar que se hayan obtenido resultados en la consulta
                    res.status(500).send({
                        message: 'NO SE ENCONTRÓ NINGUN POST CON LA ID INDICADA'
                    })
                }else{
                    res.status(200).json(result.rows);
                }
            }

        })

    },

    getPost: (req: any,res: any)=>{ // Método para obtener posts

        // Consulta para seleccionar datos de la base de datos
        db.query(`SELECT * FROM POSTS WHERE ID_POST='${req.params.id}'`,(err: any,result: any)=>{

            if(err){ // Condicional para mostara un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            }else{
                if(result.rowCount===0){ // Condicional para verificar que se hayan obtenido resultados en la consulta
                    res.status(400).send({
                        message: 'NO SE ENCONTRÓ NINGUN POST CON LA ID INDICADA'
                    })
                }else{
                    res.status(200).json(result.rows);
                }
            }
        })

    },

    orderPost: (req: any,res: any)=>{ // Método para ordenamiento de posts

        // Consulta para seleccionar datos de la base de datos
        db.query(`SELECT * FROM POSTS ORDER BY DATE DESC`,(err: any, result: any)=>{   

            if(err){ // Condicional para mostar un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
            });
        }else{
            res.status(200).json(result.rows);
        }
    })  

    }
    
}