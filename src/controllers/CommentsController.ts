export{}
const db = require('../database/db');

module.exports={

    createComment: (req: any, res: any)=>{ // Método para crear comentarios
        // Consulta para buscar el ID del post en la  datos
        db.query(`SELECT * FROM COMMENTS WHERE ID_POST='${req.body.id_post}'`, (err: any,result: any)=>{

            if(err){ // Condicional para mostrar un error
                res.status(500).send({
                    message: 'ERROR'
                })
            }else{
                if(result.rowCount!==0){ // Condicional para verificar que la consulta no esté vacía
                    if(req.body.conten!=='' && req.body.id_post!==''){ // Condicional para validar que se ingresen todos los datos
                        // Consulta para insertar datos en la base de datos
                        db.query(`INSERT INTO COMMENTS (CONTENT, ID_POST) VALUES ('${req.body.content}', '${req.body.id_post}')`, (err: any,result: any)=>{
                        if(err){ // condicional para mostrar errores
                            res.status(500).send({
                                message: 'ERROR DEL SERVIDOR'
                            });
                        }else{
                        res.status(200).json(result.rows)
                        }
                        })
                    }else{ // Condicional para indicar el error que debe llenar los campos
                        res.status(400).send({
                        message: 'ERROR: DEBE LLENAR TODOS LOS CAMPOS'
                    })
                    }
                }else{ // Condicional para indicar que no se encontró ningun post con el id
                    res.status(400).send({
                    message: 'NO SE ENCONTRÓ NINGUN POST CON EL ID INDICADO'
                })
                }
            }

        })
    },

    deleteComment:(req: any,res: any)=>{ // Método para eliminar un comentario

        // Consulta para eliminar un comentario de la base de datos
        db.query(`DELETE FROM COMMENTS WHERE ID_COMMENT='${req.params.id}'`,(err: any,result: any)=>{
            
            if(err){ // Condicional para mostrar un error

                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });

            }else{
                if(result.rowCount===0){ // Condicional para verificar que la consulta no esté vacía
                    res.status(500).send({
                        message: 'NO SE ENCONTRÓ NINGUN COMENTARIO CON LA ID INDICADA'
                    })
                }else{
                    res.status(200).json(result.rows);
                }
            }

        })

    },

    getComment:(req: any,res: any)=>{ // Método para obtener comentarios

        // Consulta para obtener un comentario en la base de datos
        db.query(`SELECT * FROM COMMENTS WHERE ID_COMMENT='${req.params.id}'`,(err: any,result: any)=>{

            if(err){ // Condicional para mostrar un error
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });

            }else{ 
                if(result.rowCount===0){ // Condicional para verificar que la consulta no esté vacía
                    res.status(500).send({
                        message: 'NO SE ENCONTRÓ NINGUN COMENTARIO CON LA ID INDICADA'
                    })
                }else{
                    res.status(200).json(result.rows);
                }
            }

        })

    },

    getAllComments:(req: any,res: any)=>{ // Método para obtener todos los comentarios de un post

        // Consulta para obtener comentarios
        db.query(`SELECT * FROM COMMENTS WHERE ID_POST='${req.params.id}'`,(err: any,result: any)=>{

            if(err){ // Condicional para mostrar errores
                res.status(500).send({
                    message: 'ERROR DEL SERVIDOR'
                });
            
            }else{ 
                if(result.rowCount===0){ // Condicional para verificar que la consulta no esté vacía
                    res.status(500).send({
                        message: 'NO SE ENCONTRÓ NINGUN COMENTARIO EN EL POST'
                    })
                }else{
                    res.status(200).json(result.rows);
                }
            }

        })

    }

}