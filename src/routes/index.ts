export{}
const express = require('express');
const router = express.Router();
const postsRouter = require('./PostsRouter');
const commentsRouter = require('./CommentsRouter');

router.use('/posts',postsRouter); // Definir el enrutador para métodos de posts
router.use('/comments',commentsRouter); // Definir el enrutador para métodos de comments

module.exports=router; // exportamos las rutas