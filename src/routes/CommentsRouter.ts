export{}
const express = require('express');
const router = express.Router();
const commentsController = require('../controllers/CommentsController');

router.post('/create',commentsController.createComment) // Endpoint para la creación de comentarios

router.delete('/delete/:id',commentsController.deleteComment) // Endpoint para la eliminación de comentarios

router.get('/get/:id',commentsController.getComment) // Endpoint para la obtención de comentarios

router.get('/all/:id',commentsController.getAllComments) // Endpoint para la obtención de todos los comentarios de un post

module.exports=router; // Exportamos las rutas