"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var router = express.Router();
var postsController = require('../controllers/PostsController');
router.post('/create', postsController.createPost); // Endpoint para creación de posts
router.delete('/delete/:id', postsController.deletePost); // Endpoint para eliminación de posts
router.get('/get/:id', postsController.getPost); // Endpoint para obtención de posts
router.put('/update/:id', postsController.updatePost); // Endpoint para actualización de posts
router.get('/order', postsController.orderPost); // Endpoint para ordenamiento de posts
module.exports = router; // Exportamos las rutas
