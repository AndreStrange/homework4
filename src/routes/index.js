"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var router = express.Router();
var postsRouter = require('./PostsRouter');
var commentsRouter = require('./CommentsRouter');
router.use('/posts', postsRouter); // Definir el enrutador para métodos de posts
router.use('/comments', commentsRouter); // Definir el enrutador para métodos de comments
module.exports = router; // exportamos las rutas
