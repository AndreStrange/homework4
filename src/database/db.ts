const {Pool} = require('pg'); // módulo postgresql

const connectionDB = new Pool({ // Establecemos conexión con la base de datos

  user: 'postgres',
  host: 'localhost',
  database: 'blogs',
  password: '123',
  port: 5432,

})

module.exports=connectionDB; // Exportamos la conexión