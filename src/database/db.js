"use strict";
var Pool = require('pg').Pool; // módulo postgresql
var connectionDB = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'blogstest',
    password: '123',
    port: 5432,
});
module.exports = connectionDB; // Exportamos la conexión
